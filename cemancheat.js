// ==UserScript==
// @name         CémanCheat
// @namespace    http://tampermonkey.net/
// @version      3.4
// @description  Crack learning project for cemantix.
// @author       megaloadon
// @match        https://cemantix.certitudes.org/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=certitudes.org
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function() {
    'use strict';

    // Set wordlist
    GM.xmlHttpRequest({ method: "GET", url: "https://raw.githubusercontent.com/clem9669/wordlists/master/dictionnaire_fr_small", headers: { "Accept": "text/xml" }, responseType: "document",
                       onload: function(response) {
                           // Inject responseXML into existing Object (only appropriate for XML content).
                           const initial_wordlist = response.responseText.split('\n');
                           var final_wordlist = [];

                           // Filter
                           for(let i = 0; i < initial_wordlist.length; i++) {
                               const word = initial_wordlist[i];
                               if (!word.includes(' ') && !word.includes("'")) {
                                   final_wordlist.push(word);
                               }
                           }

                           // Assign to final wordlist
                           GM_setValue('base_wordlist',final_wordlist);

                           // Debug
                           console.log('[+] :: Downloaded wordlist ('+final_wordlist.length+' words)');
                       }
                      });

    // Input the word and click submit
    function test_word(word) {
        document.getElementById("cemantix-guess").value = word;
        document.getElementById("cemantix-guess-btn").click();
    }

    // Get synonyms
    function download_lex_for(word, wordlist, wordlist_gm) {
        GM.xmlHttpRequest({ method: "GET", url: "https://www.rimessolides.com/motscles.aspx?m=" + word, headers: { "Accept": "text/xml" }, responseType: "document",
                           onload: function(response) {
                               GM_setValue('run_lex',true);
                               // Quit if no synonyms found
                               if (response.responseText.includes("votre recherche ne contient aucun résultat")) { console.log('no syn'); return; }

                               // Adding to synonyms
                               const initial_split = response.responseText.split('<a class="l-black" href="motscles.aspx?m=');
                               let count = 0;
                               for (let i = 1; i < initial_split.length; i++) {
                                   // Get word
                                   const w = decodeURI(initial_split[i].split('"')[0]);
                                   // Sanitize
                                   const ba = document.getElementById('cemantix-guesses');
                                   if (!w.includes('+') && !w.includes("'") && !ba.innerHTML.includes(w) && !wordlist.includes(w)) {
                                       // Add
                                       count++;
                                       wordlist.unshift(w);
                                       GM_setValue(wordlist_gm, wordlist);
                                       console.log('    + [LEX](' + word + ') added ' + w);
                                   }
                               }
                               if (count == 0){console.log('    - No words added from lex.')}else{console.log('    - Added ' + count + ' word(s) from lex.')};
                               GM_setValue('run_lex',false);
                           }
                          });
    }

    // DEBUG
    // Reset highscore
    GM_setValue('highest_score', 0.0);
    // Reset synonym history
    GM_setValue('searched_synonyms', []);
    // Reset synonym wordlist
    GM_setValue('syno_wordlist',[]);
    // Running (wait for downloads)
    GM_setValue('run_lex',false);

    // Create the crack button
    let crack_button = document.createElement("INPUT");
    crack_button.type = "submit";
    crack_button.value = "Crack";
    crack_button.className = "guess-btn";
    crack_button.id = "crack-btn";
    document.getElementById("cemantix-form").prepend(crack_button);
    crack_button.onclick = () => {
        const startTime = performance.now();
        let ended = false;
        const refreshIntervalId = setInterval(function(){
            if (ended){clearInterval(refreshIntervalId);return;};
            if (GM_getValue('run_lex', false)) {return};

            // Get word
            let base_wordlist = GM_getValue('base_wordlist', ['error']);
            let syno_wordlist = GM_getValue('syno_wordlist', []);
            let word = "";
            let using_synonym = false;
            if (syno_wordlist.length > 0) { word = syno_wordlist[0]; syno_wordlist.shift(); GM_setValue('syno_wordlist', syno_wordlist); using_synonym = true; }
            else { word = base_wordlist[0]; base_wordlist.shift(); GM_setValue('base_wordlist', base_wordlist); }

            // Test it
            test_word(word);

            // Debug info
            console.log('[i] :: Remaining : base['+base_wordlist.length+'] | syno['+syno_wordlist.length+'] | ' + word);

            // Get match words and compare them
            const ba = document.getElementById('cemantix-guesses');
            if ( ba.innerHTML.includes('word') ) {
                // First = 1
                let index = 0;
                let answer = ba.innerHTML.split('<td class=')[3].split('>')[1].split('</td')[0];
                let new_word = ba.innerHTML.split('<td class="word')[1].split('>')[1].split('</td')[0];
                let searched_synonyms = GM_getValue('searched_synonyms', []);
                let highscore = GM_getValue('highest_score',0.0);

                // While there is a word
                while (answer) {
                    index++; // Get the next one and check it
                    if (!ba.innerHTML.split('<td class=')[3 + ((index - 1) * 5)]){break};
                    answer = ba.innerHTML.split('<td class=')[3 + ((index - 1) * 5)].split('>')[1].split('</td')[0];
                    new_word = ba.innerHTML.split('<td class="word')[index].split('>')[1].split('</td')[0];
                    if (answer && !searched_synonyms.includes(new_word)) {
                        const score = parseFloat(answer.replace(',','.'));

                        // End on win
                        if (score == 100){
                            const endTime = performance.now();
                            const seconds = (endTime - startTime) / 1000;
                            console.log('[SUCCESS] :: Found in ' + Math.floor(seconds / 60) + ' minute(s) and ' + Math.floor(seconds % 60) + ' seconds.');
                            ended = true;
                            return;
                        }

                        // Check score
                        if (score > highscore || !using_synonym) {
                            console.log('\r[+] :: new highscore for '+ new_word +' : ' + highscore + ' < ' + score + ' || false == ' + using_synonym);
                            GM_setValue('highest_score', score);
                            highscore = score;
                            // Search for synonyms and add them to wordlist
                            GM_setValue('run_lex',true);
                            download_lex_for(new_word, syno_wordlist, 'syno_wordlist');
                            using_synonym = true;
                            searched_synonyms.push(new_word);
                            GM_setValue('searched_synonyms', searched_synonyms);
                        } else { break; } // Break if score is negative
                    }
                }
            }

            // End on wordlist end
            if (base_wordlist.length == 0 && syno_wordlist.length == 0) { return; };
        },400 /* Interval Time 400ms */);
    }
})();
