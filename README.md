# CémanCheat
Est un script TemperMonkey pour tricher au cémantix, réalisé dans le but d'apprendre javascript, et parce que faire un script capable de jouer à un jeu, c'est drôle.

## Utilisation
Une fois sur le site de cémantix, un bouton "Crack" apparait à côté de l'input. Cliquez dessus et le script se lancera. Des logs sont disponibles dans la console.